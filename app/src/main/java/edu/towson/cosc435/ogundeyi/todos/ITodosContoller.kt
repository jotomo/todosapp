package edu.towson.cosc435.ogundeyi.todos

import edu.towson.cosc435.ogundeyi.todos.TodoRepository
import kotlinx.coroutines.CoroutineScope


interface ITodosController : CoroutineScope {
    val todosRepo: TodoRepository
    fun toggleCompleted(pos: Int)
    fun editTodo(pos: Int)
    suspend fun deleteTodo(pos: Int)
}