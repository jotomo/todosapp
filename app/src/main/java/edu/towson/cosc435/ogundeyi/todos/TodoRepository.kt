package edu.towson.cosc435.ogundeyi.todos

import android.content.Context
import androidx.room.Room
import edu.towson.cosc435.ogundeyi.todos.Todo
import edu.towson.cosc435.ogundeyi.todos.TodoDatabase
import java.util.*

interface ITodoRepository {
    fun GetTodos(): List<Todo>
    fun GetTodo(pos: Int): Todo
    fun AddTodo(todo: Todo)
    fun UpdateTodo(pos: Int, todo: Todo)
    fun DeleteTodo(pos: Int)
    fun GetCount(): Int
}

class TodoRepository(ctx: Context) : ITodoRepository {
    private val todos = mutableListOf<Todo>()

    private val db: TodoDatabase

    init {
        db = Room.databaseBuilder(
            ctx,
            TodoDatabase::class.java,
            "todos.db"
        )
            .allowMainThreadQueries() // avoid coroutines for now
            .fallbackToDestructiveMigration() // destroy the db when upgrading
            .build()
        // seed the db if empty
        if(db.todoDao().GetAll().isEmpty()) {
            (0..10).forEach { i ->
                db.todoDao().AddTodo(
                    Todo(
                        content = "Content $i",
                        completed = false,
                        title = "Title $i",
                        id = UUID.randomUUID(),
                        create_date = Date()
                    )
                )
            }
        }
        todos.addAll(db.todoDao().GetAll())
    }

    override fun GetTodos(): List<Todo> {
        if(todos.size == 0) {
            todos.addAll(db.todoDao().GetAll())
        }
        return todos
    }

    override fun GetTodo(pos: Int): Todo {
        return todos[pos]
    }

    override fun AddTodo(todo: Todo) {
        if(db.todoDao().Exists(todo.id)) {
            db.todoDao().UpdateTodo(todo)
        } else {
            db.todoDao().AddTodo(todo)
        }
        todos.clear()
        todos.addAll(db.todoDao().GetAll())
    }

    override fun UpdateTodo(pos: Int, todo: Todo) {
        db.todoDao().UpdateTodo(todo)
        todos.clear()
        todos.addAll(db.todoDao().GetAll())
    }

    override fun DeleteTodo(pos: Int) {
        val todo = todos[pos]
        db.todoDao().DeleteTodo(todo)
        todos.clear()
        todos.addAll(db.todoDao().GetAll())
    }

    override fun GetCount(): Int {
        return todos.size
    }
}