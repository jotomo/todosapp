package edu.towson.cosc435.ogundeyi.todos

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.gson.Gson
import edu.towson.cosc435.ogundeyi.todos.Todo
import edu.towson.cosc435.ogundeyi.todos.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_new_todo.*
import java.util.*

class NewTodoActivity : AppCompatActivity() {

    companion object {
        val TAG = NewTodoActivity::class.java.simpleName
        const val TODO = "todo"
        const val POSITION = "position"
    }

    private var todo: Todo? = null
    private var position = -1

    private fun makeTodo() : Todo {
        return Todo(
            content = text_edit_text.editableText.toString(),
            title = title_edit_text.editableText.toString(),
            completed = newtodo_completed_checkbox.isChecked,
            id = if(todo == null) { UUID.randomUUID() } else { todo!!.id },
            create_date = if(todo == null) { Date() } else { todo!!.create_date }
        )
    }

    private fun saveTodo() {
        val todo = makeTodo()
        val intent = Intent()
        intent.putExtra(TODO, Gson().toJson(todo))
        intent.putExtra(POSITION, position)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_todo)

        save_btn.setOnClickListener { saveTodo() }

        if(intent.hasExtra(TODO)) {
            populateForm()
        }
    }

    private fun populateForm() {
        val json = intent.getStringExtra(TODO)
        position = intent.getIntExtra(POSITION, -1)
        todo = Gson().fromJson<Todo>(json, Todo::class.java)
        title_edit_text.editableText.clear()
        title_edit_text.editableText.append(todo?.title)
        text_edit_text.editableText.clear()
        text_edit_text.editableText.append(todo?.content)
        newtodo_completed_checkbox.isChecked = if(todo == null) { false } else { todo!!.completed }
    }

    override fun onResume() {
        super.onResume()
        recyclerView?.adapter?.notifyDataSetChanged()
    }
}
