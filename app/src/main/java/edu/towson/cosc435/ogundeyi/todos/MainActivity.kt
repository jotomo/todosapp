package edu.towson.cosc435.ogundeyi.todos

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import edu.towson.cosc435.ogundeyi.todos.Todo
import edu.towson.cosc435.ogundeyi.todos.R
import edu.towson.cosc435.ogundeyi.todos.TodoAdapter
import edu.towson.cosc435.ogundeyi.todos.ITodoRepository
import edu.towson.cosc435.ogundeyi.todos.TodoRepository
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.cancel
import kotlin.coroutines.CoroutineContext

class MainActivity : AppCompatActivity(), ITodosController {

    private fun showSpinner(){
    prograssBar.visibility = View.VISIBLE
    }

    private fun hideSpinner(){
        prograssBar.visibility = View.GONE
    }
    override lateinit var todosRepo: TodoRepository

    override fun toggleCompleted(pos: Int) {
        val todo = todosRepo.GetTodo(pos)
        todo.completed = !todo.completed
        todosRepo.UpdateTodo(pos, todo)
        recyclerView.adapter?.notifyItemChanged(pos)
    }

    override fun editTodo(pos: Int) {
        val intent = Intent(this, NewTodoActivity::class.java)
        val todo = todosRepo.GetTodo(pos)
        intent.putExtra(NewTodoActivity.TODO, Gson().toJson(todo))
        intent.putExtra(NewTodoActivity.POSITION, pos)
        startActivityForResult(intent, REQUEST_CODE)
    }

    override val coroutineContext: CoroutineContext
        get() = lifecycleScope.coroutineConext

    override suspend fun deleteTodo(pos: Int) {
        confirmDelete {
            todosRepo.DeleteTodo(pos)
            recyclerView.adapter?.notifyItemRemoved(pos)
        }
    }

    private fun confirmDelete(callback: () -> Unit) {
        AlertDialog
            .Builder(this)
            .setMessage("Delete Todo?")
            .setPositiveButton("Delete") { _, _ ->
                callback()
            }
            .setNegativeButton("Cancel") { _, _ ->

            }
            .create()
            .show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        todosRepo = TodoRepository(this)

        add_todo_btn.setOnClickListener { launchNewTodoActivity() }
        recyclerView.adapter = TodoAdapter(this)
        recyclerView.layoutManager = LinearLayoutManager(this)
    }

    private fun launchNewTodoActivity() {
        val intent = Intent(this, NewTodoActivity::class.java)
        startActivityForResult(intent,
            REQUEST_CODE
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            REQUEST_CODE -> {
                when(resultCode) {
                    Activity.RESULT_OK -> {
                        val todo = Gson().fromJson<Todo>(data?.getStringExtra(NewTodoActivity.TODO), Todo::class.java)
                        val pos = data?.getIntExtra(NewTodoActivity.POSITION, -1)
                        // if there is no position, assume this is a new Todo
                        if(pos == null || pos == -1) {
                            todosRepo.AddTodo(todo)
                            val count = todosRepo.GetCount()
                            recyclerView.scrollToPosition(count)
                            recyclerView.adapter?.notifyItemInserted(count)
                        } else {
                            // otherwise, we are updating
                            todosRepo.UpdateTodo(pos, todo)
                            recyclerView.scrollToPosition(pos)
                            recyclerView.adapter?.notifyItemChanged(pos)
                        }
                    }
                }
            }
        }
    }

    companion object {
        const val REQUEST_CODE = 1
        val TAG = MainActivity::class.java.simpleName
    }

    override fun onStop(){
        super.onStop()
        this.cancel()
    }
}