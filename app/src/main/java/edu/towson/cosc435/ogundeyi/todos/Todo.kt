package edu.towson.cosc435.ogundeyi.todos

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*


@Entity
data class Todo(
    var content: String,
    var completed: Boolean,
    var title: String,
    var create_date: Date,
    @PrimaryKey
    var id: UUID)