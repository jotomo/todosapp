package edu.towson.cosc435.ogundeyi.todos

import androidx.room.*
import edu.towson.cosc435.ogundeyi.todos.Todo
import java.util.*

@Dao
interface TodoDao {
    @Query("select * from todo")
    suspend fun GetAll(): List<Todo>
    @Insert
    suspend fun AddTodo(todo: Todo)
    @Update
    suspend fun UpdateTodo(todo: Todo)
    @Delete
    suspend fun DeleteTodo(todo: Todo)
    @Query("select count(*) from todo where id = :todo")
    suspend fun Exists(todo: UUID): Boolean
}

@Database(entities = [Todo::class], version = 2)
@TypeConverters(Converters::class)
abstract class TodoDatabase : RoomDatabase() {
    abstract fun todoDao(): TodoDao
}



class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }

    @TypeConverter
    fun fromStringToUUID(value: String): UUID {
        return UUID.fromString(value)
    }

    @TypeConverter
    fun uuidToString(uuid: UUID): String {
        return uuid.toString()
    }
}

