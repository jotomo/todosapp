package edu.towson.cosc435.ogundeyi.todos

import android.graphics.Paint
import android.graphics.PaintFlagsDrawFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import edu.towson.cosc435.ogundeyi.todos.Todo
import edu.towson.cosc435.ogundeyi.todos.R
import edu.towson.cosc435.ogundeyi.todos.ITodosController
import kotlinx.android.synthetic.main.todo_listitem_layout.view.*

class TodoAdapter(val controller: ITodosController) : RecyclerView.Adapter<TodoViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.todo_cardview_layout, parent, false)
        val vh = TodoViewHolder(view)

        view.checkbox.setOnClickListener {
            val position = vh.adapterPosition
            controller.toggleCompleted(position)
        }

        view.setOnClickListener {
            controller.editTodo(vh.adapterPosition)
        }

        view.setOnLongClickListener {
            controller.deleteTodo(vh.adapterPosition)
            true
        }

        return vh
    }

    override fun getItemCount(): Int {
        return controller.todosRepo.GetCount()
    }

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        val todo = controller.todosRepo.GetTodo(position)
        val tv = holder.itemView.todo_title
        updateStrikethrough(todo, tv)
        holder.itemView.todo_title.text = todo.title
        holder.itemView.todo_text.text = todo.content
        holder.itemView.checkbox.isChecked = todo.completed
        holder.itemView.todo_create_date.text = todo.create_date.toString()
    }

    private fun updateStrikethrough(todo: Todo, tv: TextView) {
        if(todo.completed) {
            tv.paintFlags = tv.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        } else {
            tv.paintFlags = tv.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
        }
    }
}

class TodoViewHolder(view: View) : RecyclerView.ViewHolder(view)